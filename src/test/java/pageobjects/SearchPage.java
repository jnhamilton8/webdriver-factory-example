package pageobjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static jnhamilton.selenium.setup.TestConfiguration.applicationUrl;

public class SearchPage {

    private WebDriver driver;
    private static final int TIMEOUT_IN_SECONDS = 2;
    private static final By GOOGLE_SEARCH_TERMS_AGREE_BUTTON = By.xpath("//div[contains(text(),'I agree')]");

    final By SEARCH_TEXT_FIELD = By.name("q");
    final By GOOGLE_LOGO = By.xpath("//div[contains(@class,'logo')]");
    private Wait<WebDriver> wait;

    public SearchPage(WebDriver driver) {
        this.driver = driver;
        if (!driver.getCurrentUrl().contains(applicationUrl)) {
            throw new WebDriverException("We are not on the Google Search Page");
        }
    }

    public void enterSearchTermAndSubmitSearch(String text) {
        cleanGoogleSearchConfirmationIfExists();

        WebElement searchButton = driver.findElement(SEARCH_TEXT_FIELD);
        searchButton.sendKeys(text);
        searchButton.sendKeys(Keys.ENTER);
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_IN_SECONDS);
        wait.until(ExpectedConditions.visibilityOfElementLocated(GOOGLE_LOGO));
    }

    private void cleanGoogleSearchConfirmationIfExists() {
        if (isElementPresent(GOOGLE_SEARCH_TERMS_AGREE_BUTTON, 2)) {
            driver.findElement(GOOGLE_SEARCH_TERMS_AGREE_BUTTON).click();
        }
    }

    private boolean isElementPresent(By selector, int timeout) {
        wait = returnFluentWait(timeout, 500);
        try {
            return wait.until(ExpectedConditions.visibilityOfElementLocated(selector)).isDisplayed();
        } catch (TimeoutException ex) {
            return false;
        }
    }

    private Wait<WebDriver> returnFluentWait(int timeout, int poll) {
        return new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(timeout))
                .pollingEvery(Duration.ofMillis(poll))
                .ignoring(java.util.NoSuchElementException.class);
    }
}
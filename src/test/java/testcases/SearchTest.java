package testcases;

import jnhamilton.selenium.setup.TestConfiguration;
import jnhamilton.selenium.setup.TestDriverFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import pageobjects.SearchPage;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class SearchTest {

    public WebDriver driver;

    @BeforeEach
    public void setUp() {
        driver = new TestDriverFactory().createDriver();
        driver.navigate().to(TestConfiguration.applicationUrl);
    }

    @Test
    public void enterSearchTermAndVerifyResults() {
        SearchPage searchPage = new SearchPage(driver);
        searchPage.enterSearchTermAndSubmitSearch("mug cakes");
        assertThat(driver.getCurrentUrl(), containsString("mug+cakes"));
    }

    @AfterEach
    public void tearDown(){
        driver.quit();
    }
}
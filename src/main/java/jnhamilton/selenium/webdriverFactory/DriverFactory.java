package jnhamilton.selenium.webdriverFactory;

import io.github.bonigarcia.wdm.WebDriverManager;
import jnhamilton.selenium.webdriverFactory.configurations.LocalDriverConfiguration;
import jnhamilton.selenium.webdriverFactory.configurations.RemoteDriverConfiguration;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Webdriver factory class that configures and returns either a local or remote driver instance
 * as required
 */

public class DriverFactory {

    private MutableCapabilities capabilities;
    private WebDriver driver;

    /**
     * Creates a local driver, as it takes the LocalDriver Configuration object
     *
     * @param configuration the local driver configuration
     * @return instance of the local driver as configured
     */
    public WebDriver createLocalDriver(LocalDriverConfiguration configuration) {

        switch (configuration.browser) {

            case "chrome":
                createChromeDriver();
                break;

            case "ie":
                createInternetExplorerDriver();
                break;

            case "firefox":
                createFireFoxDriver();
                break;

            case "edge":
                createEdgeDriver();
                break;

            //defaults to chrome
            default:
                createChromeDriver();
                break;
        }

        driver.manage().window().maximize();
        return driver;
    }

    /**
     * Creates a remote driver, as it takes the RemoteDriver configuration object
     *
     * @param configuration the remote driver configuration
     * @return instance of the remote driver as configured
     */
    public WebDriver createRemoteDriver(RemoteDriverConfiguration configuration) {
        String remoteServer = buildRemoteServer(configuration.seleniumHubUrl, configuration.seleniumHubPort);

        switch (configuration.browser) {

            case "firefox":
                capabilities = new FirefoxOptions();
                break;

            case "chrome":
                capabilities = new ChromeOptions();
                break;

            case "internet explorer":
                capabilities = new InternetExplorerOptions();
                break;

        }

        setCapabilities(configuration.platform, configuration.browserVersion);

        try {
            driver = new RemoteWebDriver(new URL(remoteServer), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return driver;
    }

    private void setCapabilities(String platform, String browserVersion) {
        capabilities.setCapability(CapabilityType.PLATFORM_NAME, platform);
        capabilities.setCapability(CapabilityType.BROWSER_VERSION, browserVersion);
    }

    /**
     * Build a Uri for your GRID Hub instance
     *
     * @param remoteServer     The hostname or IP address of your GRID instance, include the http://
     * @param remoteServerPort Port of your GRID Hub instance
     * @return the URL as a string
     */
    public static String buildRemoteServer(String remoteServer, int remoteServerPort) {
        return String.format("%s/%d/wd/hub", remoteServer, remoteServerPort);
    }

    private WebDriver createChromeDriver() {
        WebDriverManager.chromedriver().setup();

        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-plugins");
        options.addArguments("disable-extensions");
        options.addArguments("test-type");
        driver = new ChromeDriver(options);
        return driver;
    }

    private WebDriver createFireFoxDriver() {
        WebDriverManager.firefoxdriver().setup();

        driver = new FirefoxDriver();
        return driver;
    }


    private WebDriver createInternetExplorerDriver() {
        //uses 32 bit because the 64 bit driver is SO SLOW
        WebDriverManager.iedriver().arch32().setup();

        driver = new InternetExplorerDriver();
        return driver;
    }

    private WebDriver createEdgeDriver() {
        WebDriverManager.edgedriver().setup();

        EdgeOptions edgeOptions = new EdgeOptions();
        edgeOptions.setPageLoadStrategy(PageLoadStrategy.EAGER);
        driver = new EdgeDriver(edgeOptions);
        return driver;
    }
}